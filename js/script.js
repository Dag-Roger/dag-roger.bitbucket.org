$(document).ready(function(){
	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing');
	});

    this_year();

    // Viser årstallet i Footer
    function this_year () {
    	var d = new Date();
       	var year = d.getFullYear();
       	$("#this_year").text(year);
    }
});
