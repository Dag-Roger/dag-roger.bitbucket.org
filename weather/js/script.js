$(document).ready(function() {
    weather();
});



function weather() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var localLatitude = position.coords.latitude;
      var localLongitude = position.coords.longitude;

      var link = "http://api.openweathermap.org/data/2.5/weather?lat="+ localLatitude + "&lon=" + localLongitude +"&appid=e9088d7d0e090e6aba73d5cff6ae7754";

      var html = "";

      $.getJSON(link, function(result){
        var kelvin = result.main.temp;
        var celsius = Math.floor(kelvin - 273.15);
        var fahrenheit = Math.floor(kelvin * 1.8 - 459.67);
        var condition = result.weather[0].description;

        $(".city").html(result.name);
        $(".country").html(result.sys.country);

        $(".temp").html(celsius);
        $(".t").html("C");

        $(".condition").html(condition);
        $(".icon").html('<img class="WeatherIcon" src="http://openweathermap.org/img/w/' + result.weather[0].icon + '.png"/>');


        $( "button" ).click(function() {
          var t = $(".t").text();
          if (t == "C") {
            $(".temp").html(fahrenheit);
            $(".t").html("F");
          } else {
            $(".temp").html(celsius);
            $(".t").html("C");
          }
        });
      });
    });
  }
}
