// $( document ).ready(function() {
//     $.getJSON( "quotes.json", function( data ) {
//       var items = [];
//       $.each( data, function( key, val ) {
//         items.push( "<li id='" + key + "'>" + val + "</li>" );
//       });
//
//       $( "<ul/>", {
//         "class": "my-new-list",
//         html: items.join( "" )
//       }).appendTo( "body" );
//     });
// });
function ranQuote() {
    $.getJSON("json/quotes.json", function(json) {

        var html = "";
        var twitter = "";
        var ranNr = Math.floor((Math.random() * 10) + 1);

        json = json.filter(function(val) {
            return (val.id == ranNr);
        });

        json.forEach(function(val) {
            var keys = Object.keys(val);
            // html += "<blockquote>";

                html += "<h2>" + val.Quote + "</h2>";
                html += "<footer>- " + val.Author + "</footer>";

            // html += "</blockquote>";

            twitter += '<a class="btn btn-primary btn-lg btn-block" ';
            twitter += 'href="https://twitter.com/intent/tweet?text=';

                twitter +=  val.Quote + ' -' + val.Author + ' #quotes';

            twitter += ' " role="button" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>';

        });

        $(".message").html(html);
        $(".twitter-box").html(twitter);
        $('body').css('background',randomColor());
   });
}


function randomColor() {
    var colors = ['#16a085', '#27ae60', '#b14a5f', '#f39c12', '#e74c3c', '#9b59b6', '#FB6964', '#4c4299', "#ba5a4c", "#BDBB99", "#77B1A9", "#73A857"];
    var ranNr = Math.floor((Math.random() * 10) + 1);

    return colors[ranNr];
}

$(document).ready(function() {

    $('body').css('background',randomColor());

    ranQuote();

    $("#getMessage").on("click", function() {

        ranQuote();

    });

});
